/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

/**
 *
 * @author estudiante
 */
public class Foto {
    private String pathToFile;
    private String tipoArchivo;
    private float tamano;

    public Foto() {
    }
    
    public Foto(String pathToFile, String tipoArchivo, float tamano) {
        this.pathToFile = pathToFile;
        this.tipoArchivo = tipoArchivo;
        this.tamano = tamano;
    }

    public String getPathToFile() {
        return pathToFile;
    }

    public void setPathToFile(String pathToFile) {
        this.pathToFile = pathToFile;
    }

    public String getTipoArchivo() {
        return tipoArchivo;
    }

    public void setTipoArchivo(String tipoArchivo) {
        this.tipoArchivo = tipoArchivo;
    }

    public float getTamano() {
        return tamano;
    }

    public void setTamano(float tamano) {
        this.tamano = tamano;
    }

    @Override
    public String toString() {
        return "Foto{" + "pathToFile=" + pathToFile + ", tipoArchivo=" + tipoArchivo + ", tamano=" + tamano + '}';
    }
    
    
}
