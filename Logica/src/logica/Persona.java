/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.util.LinkedList;

/**
 *
 * @author estudiante
 */
public class Persona {
    private String nombre;
    private String apellidos;
    private LinkedList <Perfil> listaPerfiles;
    private LinkedList <Lugar> listaLugares ;
    private Foto foto;

    public Persona() {
    }

    
//    public Persona(String nombre, String apellidos, LinkedList<Lugar> listaLugares, Foto foto) {
//        this.nombre = nombre;
//        this.apellidos = apellidos;
//        this.listaLugares = listaLugares;
//        this.foto = foto;
//        listaPerfiles = new LinkedList<>();
//    }
    

    public Persona(String nombre, String apellidos,Lugar lugar, Perfil perfil) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.listaLugares = new LinkedList<>();
        this.listaPerfiles= new LinkedList<>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public LinkedList<Perfil> getListaPerfil() {
        return listaPerfiles;
    }

    public void setListaPerfil(LinkedList<Perfil> listaPerfiles) {
        this.listaPerfiles = listaPerfiles;
    }

    public LinkedList<Lugar> getListaLugar() {
        return listaLugares;
    }

    public void setListaLugar(LinkedList<Lugar> listaLugares) {
        this.listaLugares = listaLugares;
    }

    public Foto getFoto() {
        return foto;
    }

    public void setFoto(Foto foto) {
        this.foto = foto;
    }
    
   
    /**
     * 
     * @param lugar 
     */
    public void agregarLugar(Lugar lugar){
         listaLugares.add(lugar);
    }
    /**
     * 
     * @param perfil 
     */
    public void agregarPerfil(Perfil perfil){
        listaPerfiles.add(perfil);
    }
    


    
    @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre + ", apellidos=" + apellidos + ", listaPerfil=" + listaPerfiles +
                ", listaLugar=" + listaLugares + ", foto=" + foto + '}';
    }
    
}
