/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

/**
 *
 * @author estudiante
 */
public class Lugar {
    private String nombreLugar;
    private String direccion;
    private String telefono;

    public Lugar() {
    }

    
    public Lugar(String nombreLugar, String direccion, String telefono) {
        this.nombreLugar = nombreLugar;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public String getnombreLugar() {
        return nombreLugar;
    }

    public void setnombreLugar(String nombreLugar) {
        this.nombreLugar = nombreLugar;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return "Lugar{" + "nombre=" + nombreLugar + ", direccion=" + direccion + ", telefono=" + telefono + '}';
    }
    
    
}
