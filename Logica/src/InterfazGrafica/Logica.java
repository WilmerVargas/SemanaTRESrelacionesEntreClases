/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfazGrafica;

import java.util.LinkedList;
import logica.*;

/**
 *
 * @author estudiante
 */
public class Logica {
    private LinkedList<Persona> personas;
    private LinkedList<Lugar> lugares;
    private LinkedList<Perfil> perfiles;
    
    public Logica() {
       personas = new LinkedList<>();
       lugares = new LinkedList<>();
       perfiles = new LinkedList<>();
    }
    
    public void agregarPersona(Persona p) {
        personas.add(p);
    }
    /**
     * agrega un lugar a la lista
     * @param newLugar lugar que se agrega
     */
    public void agregarLugar (Lugar newLugar){
        lugares.add(newLugar);
    }
    public void agregarPerfil (Perfil newPerfil){
        perfiles.add(newPerfil);
    }

    public LinkedList<Persona> getPersonas() {
        return personas;
    }
    public LinkedList<Lugar> getLugares() {
        return lugares;
    }
    public LinkedList<Perfil> getPerfiles() {
        return perfiles;
    }

    public void setPersonas(LinkedList<Persona> personas) {
        this.personas = personas;
    }
    
}
